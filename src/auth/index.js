import axios from 'axios';
import { router } from '../main'
import Vue from 'vue'

const API_URL = 'https://api.smithwienswedding.com/login'

export default {
  user: {
    authenticated: false
  },
  loginUser(creds, redirect) {
    axios.post(API_URL, creds)
      .catch((err) => {
        console.error(err)
      })
      .then((response) => {
        if(response.data.success === false) {
          Vue.toasted.show("Sorry boo that's the wrong password 😘",
            { duration: 2000, position: 'bottom-center', type: 'error', theme: 'outline' })
          console.log(response.data.message);
        } else {
          localStorage.setItem('id_token', response.data.token)
          this.user.authenticated = true
          if(redirect) {
            router.push(redirect)
          }
        }
      })
  },
  logout() {
    localStorage.removeItem('id_token')
    this.user.authenticated = false
  },
  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  }
}
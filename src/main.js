import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Toasted from 'vue-toasted'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import auth from './auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Login
  },
  {
    path: '/charts',
    component: Home,
    beforeEnter: (to, from, next) => {
      let authenticated = auth.user.authenticated;
        console.log(authenticated);
      if(authenticated) {
        next()
      } else {
        next('/')
      }
    }
  }
]

Vue.use(Toasted)

export const router = new VueRouter({
  routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
